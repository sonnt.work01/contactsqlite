package com.bkacad.nnt.todosqlitedemo.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bkacad.nnt.todosqlitedemo.db.DAO;
import com.bkacad.nnt.todosqlitedemo.db.DBHelper;

import java.util.ArrayList;
import java.util.List;

public class ContactDAO implements DAO<Contact> {

    private DBHelper dbHelper;

    public ContactDAO(DBHelper dbHelper){
        this.dbHelper = dbHelper;
    }

    @Override
    public List<Contact> all() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "SELECT * FROM contacts";
        Cursor cursor = db.rawQuery(sql, null);
        // Đưa con trỏ về đầu hàng
        List<Contact> list = new ArrayList<>();
        if(cursor.moveToFirst()){
            int idIndex = cursor.getColumnIndex("id");
            int nameIndex = cursor.getColumnIndex("name");
            int addressIndex = cursor.getColumnIndex("address");
            int phoneIndex = cursor.getColumnIndex("phone");
            do{
                Contact item = new Contact();
                // Lấy dữ liệu từ Sqlite -> set giá trị item
                item.setId(cursor.getLong(idIndex));
                item.setName(cursor.getString(nameIndex));
                item.setAddress(cursor.getString(addressIndex));
                item.setPhone(cursor.getString(phoneIndex));

                // Push item này vào arraylist (todo)
                list.add(item);
            }
            while(cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    @Override
    public Contact get(long id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "SELECT * FROM contacts WHERE id = "+id;
        Cursor cursor = db.rawQuery(sql, null);
        Contact item = null;
        if(cursor.moveToFirst()){
            int idIndex = cursor.getColumnIndex("id");
            int nameIndex = cursor.getColumnIndex("name");
            item  = new Contact();
            item.setId(cursor.getLong(idIndex));
            item.setName(cursor.getString(nameIndex));
        }
        return item;
    }

    @Override
    public long create(Contact item) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("name",item.getName());
        contentValues.put("address",item.getAddress());
        contentValues.put("phone",item.getPhone());
        // Lấy id bản ghi mới
        long id = db.insert("contacts", null, contentValues );

        return id;
    }

    @Override
    public int update(Contact item) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name",item.getName());
        contentValues.put("address",item.getAddress());
        contentValues.put("phone",item.getPhone());
        // VD: update cho bản ghi 1 => UPDATE todos SET title = "Hello" WHERE id = 1
        int rs = db.update("contacts",contentValues,"id = " + item.getId(), null);
        return rs;
    }

    @Override
    public int delete(long id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int rs = db.delete("contacts", "id = "+id, null);
        return rs;
    }
}
