package com.bkacad.nnt.todosqlitedemo;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.bkacad.nnt.todosqlitedemo.adapter.ContactAdapter;
import com.bkacad.nnt.todosqlitedemo.adapter.TodoAdapter;
import com.bkacad.nnt.todosqlitedemo.db.DAO;
import com.bkacad.nnt.todosqlitedemo.db.DBHelper;
import com.bkacad.nnt.todosqlitedemo.model.Contact;
import com.bkacad.nnt.todosqlitedemo.model.ContactDAO;
import com.bkacad.nnt.todosqlitedemo.model.Todo;
import com.bkacad.nnt.todosqlitedemo.model.TodoDAO;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

//    private Button btnAdd;
//    private EditText edtTodo;
//    private ListView lvTodo;
//    private List<Todo> todos;

    private FloatingActionButton fabAdd;
    private ListView lvCustom;
    private ContactAdapter contactAdapter;
    private List<Contact> contacts;
    private ContactDialog contactDialog;

    private AlertDialog alertDialog;
    private AlertDialog.Builder builder;

//    private TodoAdapter todoAdapter;

    // DBHelper và TodoDAO
    private DBHelper dbHelper;
    private DAO<Contact> contactDAO;

    private void initUI(){
//        btnAdd = findViewById(R.id.btn_main_add);
//        edtTodo = findViewById(R.id.edt_main);
//        lvTodo = findViewById(R.id.lv_main_todo);

        fabAdd = findViewById(R.id.fabAdd);
        lvCustom = findViewById(R.id.lvCustom);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUI();

        // init db
        dbHelper = new DBHelper(this);
        contactDAO = new ContactDAO(dbHelper);

        contacts = contactDAO.all();
        if(contacts.size() == 0) Toast.makeText(this, "Danh sách rỗng", Toast.LENGTH_SHORT).show();

        contactAdapter = new ContactAdapter(this,contacts);
        lvCustom.setAdapter(contactAdapter);
        // Xử lý sự kiện khi thêm vào listview

        contactDialog = new ContactDialog(this) {
            @Override
            protected void passData(String name, String address, String phone) {
//                contacts.add(new Contact(name, address, phone));
//                contactAdapter.notifyDataSetChanged();

                Contact item = new Contact(name, address, phone);
                long id = contactDAO.create(item);
                item.setId(id);
                // Push dữ liệu vào contact
                contacts.add(item);
                // Thông báo cho Adapter -> render lại view
                contactAdapter.notifyDataSetChanged();
            }
        };

        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Hiển thị lên dialog -> xử lý
                contactDialog.show();
                Toast.makeText(MainActivity.this,"Show dialog",Toast.LENGTH_SHORT).show();
            }
        });

//        btnAdd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String title = edtTodo.getText().toString();
//                if(title.isEmpty()) {
//                    edtTodo.setError("Hãy nhập dữ liệu");
//                    return;
//                }
//                edtTodo.clearFocus();
//                edtTodo.setText("");
//                // Thêm dữ liệu vào SQLite
//                Todo item = new Todo(title);
//                long id = todoDAO.create(item);
//                item.setId(id);
//                // Push dữ liệu vào todo
//                todos.add(item);
//                // Thông báo cho Adapter -> render lại view
//                todoAdapter.notifyDataSetChanged();
//            }
//        });

        lvCustom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
                intent.putExtra("data", contacts.get(position));
                startActivity(intent);
            }
        });

//        lvTodo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(MainActivity.this, todos.get(position).toString(), Toast.LENGTH_SHORT ).show();
//            }
//        });
        lvCustom.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                //
                builder = new AlertDialog.Builder(MainActivity.this).
                        setTitle("Delete?").
                        setMessage("Bạn có thực sự muốn xóa người này???").
                        setPositiveButton("Đúng", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Contact item = contacts.get(position);
                                if(contactDAO.delete(item.getId()) == 1){
                                    // XOas thanh cong -> xoa du lieu trong bo nho (RAM)
                                    contacts.remove(position);
                                    contactAdapter.notifyDataSetChanged();
                                }
                            }

                        }).setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(MainActivity.this, "Cancel deletion", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog = builder.create();
                alertDialog.show();
                Toast.makeText(getApplicationContext(), "Clickeddd", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
//        lvCustom.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//                // Xoa du lieu tai vi tri position
//                // Xoa trong SQLite
//                Contact item = contacts.get(position);
//                if(contactDAO.delete(item.getId()) == 1){
//                    // XOas thanh cong -> xoa du lieu trong bo nho (RAM)
//                    contacts.remove(position);
//                    contactAdapter.notifyDataSetChanged();
//                }
//
//                // Xoa o list => cap lai listview
//                return false;
//            }
//        });
    }

    @Override
    protected void onDestroy() {
        dbHelper.close();
        super.onDestroy();
    }
}