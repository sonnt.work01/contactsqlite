package com.bkacad.nnt.todosqlitedemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.bkacad.nnt.todosqlitedemo.R;
import com.bkacad.nnt.todosqlitedemo.model.Contact;

import java.util.List;

public class ContactAdapter extends BaseAdapter {

    private Context context;
    private List<Contact> contact;
    private Object MainActivity;

    public ContactAdapter(Context context, List<Contact> contact) {
        this.context = context;
        this.contact = contact;
    }

    @Override
    public int getCount() {
        return contact.size();
    }

    @Override
    public Object getItem(int position) {
        return contact.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.item_view,parent,false);
        }
        TextView tvName, tvPhone;
        ImageView imgPopup;
        PopupMenu popupMenu;

        tvName = convertView.findViewById(R.id.tvName);
        tvPhone = convertView.findViewById(R.id.tvPhone);
        imgPopup = convertView.findViewById(R.id.imgPopup);

        //set data
        tvName.setText(contact.get(position).getName());
        tvPhone.setText(contact.get(position).getPhone());

        // Tạo popup menu
        popupMenu = new PopupMenu(context.getApplicationContext(), imgPopup);
        popupMenu.getMenuInflater().inflate(R.menu.popup_menu,popupMenu.getMenu());

        // Sự kiện khi click vào button
        imgPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Hiển thị popup lên
                popupMenu.show();
            }
        });

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.popup_menu_details:
                        Toast.makeText(context.getApplicationContext(),"Details",Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.popup_menu_edit:
                        Toast.makeText(context.getApplicationContext(),"Edit",Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.popup_menu_delete:
                        contact.remove(position);
                        notifyDataSetChanged();
                        Toast.makeText(context.getApplicationContext(),"Delete",Toast.LENGTH_SHORT).show();
                        break;
                }
                return false;
            }
        });

        return convertView;
    }
}
